package com.techu.controllers;

import com.techu.entidad.User;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;

@RestController
public class UserController {

    private static ArrayList<User> users = new ArrayList<User>();
    static {
      users.add(new User(1, "Paloma", "paloma@gmail.com"));
      users.add(new User(2, "Vicente", "vicente@gmail.com"));
      users.add(new User(3, "Maxi", "maxi@gmail.com"));
      users.add(new User(4, "Ana", "ana@gmail.com"));
    }
    static final String BASEURL = "/apitechu/v1";

    @GetMapping(BASEURL + "/users")
    public ArrayList<User> getUsers(){
        return users;
    }

    @GetMapping(BASEURL + "/users/{id}")
    public User getUserId(@PathVariable int id){
        for(int i=0;i<users.size();i++) {
            if(users.get(i).getId() == id) {
                return users.get(i);
            }
        }
        User noUser = new User(0,"Null","Null");
        return(noUser);
    }

    @PostMapping(BASEURL + "/users")
    public User postUser(@RequestBody User newUser) {
        if(users.add(newUser)){
            return(newUser);
        } else {
            User noUser = new User(0,"Null","Null");
            return(noUser);
        }
    }

    @PutMapping(BASEURL + "/users/{id}")
    public User putUser(@PathVariable int id, @RequestBody User userToUpdate) {
        for(int i=0; i<users.size();i++){
            if(users.get(i).getId() == id){
                users.set(i, userToUpdate);
                return users.get(i);
            }
        }
        User noUser = new User(0,"Null","Null");
        return(noUser);
    }

    @DeleteMapping(BASEURL + "/users/{id}")
    public User putUser(@PathVariable int id) {
        for(int i=0; i<users.size();i++){
            if(users.get(i).getId() == id){
               users.remove(i);

            }
        }
        User noUser = new User(0,"Null","Null");
        return(noUser);
    }
}
